
DROP DATABASE IF EXISTS IEG;
CREATE DATABASE IEG;
USE IEG;

-- ----------------------------------------------------------------------------------------------------------
-- CREATION OF TABLES --
-- ----------------------------------------------------------------------------------------------------------

CREATE TABLE ingreso_egreso
(
	idIngresoEgreso VARCHAR (36) NOT NULL,
   titulo TEXT,
	descripcion TEXT,
	fecha DATE,
	monto DECIMAL (10,2),
	PRIMARY KEY (idIngresoEgreso)
);

CREATE TABLE moneda_extrangera
(
	idMonedaExtrangera VARCHAR (36) NOT NULL,
   moneda TEXT,
	descripcion TEXT,
	monto DECIMAL (10,2),
	cotizacion DECIMAL (10,2),
	PRIMARY KEY (idMonedaExtrangera)
);

USE IEG;

INSERT INTO ingreso_egreso ( idIngresoEgreso, titulo, descripcion, fecha, monto )
VALUES( "8d4t7581-d8ud-4452-ab93-52c5f4e8b6d4", "Sueldo trabajo", "Descripcion 1", '2020-06-02', 21.35 ),
	  ( "k1u75t53-22c5-47b4-b8cc-804624587647", "Sueldo trabajo", "Descripcion 2", '2020-08-15', -24.62 ) ;

SELECT * FROM ingreso_egreso;

-- *********************************************************************************************************
-- ***** QUERIES ***** --
-- *********************************************************************************************************

-- Agrupar por ganancia anual
SELECT
	YEAR(fecha) AS anio, 
	ROUND (SUM(monto), 2) AS ganancia
FROM ingreso_egreso
GROUP BY
	YEAR(fecha)

-- Agrupar por ingreso, egreso y ganancia anual
SELECT
	anio,
	ROUND (SUM(ingresos), 2) AS ingreso,	
	ROUND (SUM(egresos), 2) AS egreso,
	ROUND (SUM(ie), 2) AS ganancia
FROM
	(SELECT
		YEAR(fecha) AS anio,
		IF( monto>=0, monto, '' ) as ingresos,
		IF( monto<0, monto, '' ) as egresos,
		monto as ie
	FROM ingreso_egreso) AS subQuery
GROUP BY
	anio

-- Agrupar por ingreso, egreso y ganancia por mes de un año en particular.
SELECT
	mes,
	ROUND (SUM(ingresos), 2) AS ingreso,	
	ROUND (SUM(egresos), 2) AS egreso,
	ROUND (SUM(ie), 2) AS ganancia
FROM
	(SELECT
		YEAR(fecha) AS anio,
		MONTH(fecha) AS mes,
		IF( monto>=0, monto, '' ) as ingresos,
		IF( monto<0, monto, '' ) as egresos,
		monto as ie
	FROM ingreso_egreso) AS subQuery
WHERE
	anio = '2020'
GROUP BY
	mes
ORDER BY 
	mes

-- Agrupar por ingreso, egreso y ganancia por mes de un año en particular.
SELECT
	anio,
	mes,
	ROUND (SUM(ingresos), 2) AS ingreso,	
	ROUND (SUM(egresos), 2) AS egreso,
	ROUND (SUM(ie), 2) AS ganancia
FROM
	(SELECT
		YEAR(fecha) AS anio,
		MONTH(fecha) AS mes,
		IF( monto>=0, monto, '' ) as ingresos,
		IF( monto<0, monto, '' ) as egresos,
		monto as ie
	FROM ingreso_egreso) AS subQuery
GROUP BY
	anio,
	mes
ORDER BY 
	anio DESC,
	mes DESC

-- Obtener registros de un año en particular.
SELECT * FROM ingreso_egreso WHERE fecha >= '2020-01-01' AND fecha <= '2020-12-31';

-- Obtener registros de un año y mes en particular.
SELECT * FROM ingreso_egreso WHERE fecha >= '2021-09-01' AND fecha <= '2021-09-31';

-- ----------------------------------------------------------------------------------------------------------