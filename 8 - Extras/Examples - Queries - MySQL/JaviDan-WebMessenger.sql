
-- ----------------------------------------------------------------------------------------------------------
-- INITIALIZATION --
-- ----------------------------------------------------------------------------------------------------------

DROP DATABASE IF EXISTS javidan;
Create Database javidan;
Use javidan;

-- ----------------------------------------------------------------------------------------------------------
-- DELETE TABLES --
-- ----------------------------------------------------------------------------------------------------------

DROP TABLE photo;
DROP TABLE flag;
DROP TABLE message;
DROP TABLE subject;

-- ----------------------------------------------------------------------------------------------------------
-- CREATION OF RECORDS --
-- ----------------------------------------------------------------------------------------------------------

Create Table subject
(
	idSubject TINYINT NOT NULL AUTO_INCREMENT,
    nick varchar(30),
    publicKey TEXT,
    PRIMARY KEY (idSubject)
);

Create Table photo
(
	idPhoto TINYINT NOT NULL AUTO_INCREMENT,
    imgRemote TEXT,
    idSubject TINYINT NOT NULL,
	FOREIGN KEY (idSubject) REFERENCES subject (idSubject),
    PRIMARY KEY (idPhoto)
);

Create Table flag
(
	idFlag TINYINT NOT NULL AUTO_INCREMENT,
    stateConnection boolean,
    stateWrite boolean,
    time time,
    idSubject TINYINT NOT NULL,
	FOREIGN KEY (idSubject) REFERENCES subject (idSubject),
    PRIMARY KEY (idFlag)
);

Create Table message
(
	idMessage INT NOT NULL AUTO_INCREMENT,
    text TEXT,
    time time,
    idSubject TINYINT NOT NULL,
	FOREIGN KEY (idSubject) REFERENCES subject (idSubject),
    PRIMARY KEY (idMessage)
);

-- ----------------------------------------------------------------------------------------------------------
-- INSERTS REGISTERS --
-- ----------------------------------------------------------------------------------------------------------

INSERT INTO subject ( nick , publicKey )
VALUES ( "Nick-1" , "4jd8kslkl" ),
	   ( "Nick-2" , "u43jh5x5j" );

INSERT INTO flag ( stateConnection , stateWrite , time , idSubject )
VALUES ( false , false , null , 1 ),
	   ( false , false , null , 2 );

INSERT INTO photo ( imgRemote , idSubject )
VALUES ( null , 1 ),
	   ( null , 2 );

-- ----------------------------------------------------------------------------------------------------------
-- QUERYS --
-- ----------------------------------------------------------------------------------------------------------

SELECT * FROM subject;
SELECT * FROM photo;
SELECT * FROM flag;
SELECT * FROM message;

DROP TABLE subject;
DROP TABLE photo;
DROP TABLE flag;
DROP TABLE message;

-- ----------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------

DELETE from message where idMessage >= 0;