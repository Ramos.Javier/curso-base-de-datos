
-- ¿ Que es aggregate functions ?

-- Las funciones de agregación en SQL nos permiten efectuar operaciones sobre un conjunto de resultados, pero devolviendo un 
-- único valor agregado para todos ellos. Es decir, nos permiten obtener medias, máximos, etc... sobre un conjunto de valores.

--      COUNT: devuelve el número total de filas seleccionadas por la consulta.
--      MIN: devuelve el valor mínimo del campo que especifiquemos.
--      MAX: devuelve el valor máximo del campo que especifiquemos.
--      SUM: suma los valores del campo que especifiquemos. Sólo se puede utilizar en columnas numéricas.
--      AVG: devuelve el valor promedio del campo que especifiquemos. Sólo se puede utilizar en columnas numéricas.


-- Para estas simples pruebas, usaremos la misma estructura de datos que 'Querys basic'.


-- Creacion de tablas

Create Table cliente
(
	idCliente Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    telefono varchar(30),
    PRIMARY KEY (idCliente)
);

Create Table plato
(
	idPlato Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    precio decimal (10,2),  -- 10 digitos y 2 decimales
    PRIMARY KEY (idPlato)
);

Create Table pedido
(
	idPedido Int NOT NULL AUTO_INCREMENT,
	detalle TEXT,
	fecha datetime,
    idPlato Int NOT NULL,
    idCliente Int,
    FOREIGN KEY (idPlato) REFERENCES plato (idPlato),
    FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
	PRIMARY KEY (idPedido)
);

-- Insercion de registros.

INSERT INTO cliente ( nombre, telefono )
VALUES 	( "Diego Maradona", "1010101010" ),
	   	( "Ernesto Guevara", "1010101011" ),
	   	( "Javier Ramos", "1010101100" );

INSERT INTO plato ( nombre, precio )
VALUES 	( "Pizza", 2.52 ),
	   	( "Empanada", 5.36 ),
		( "Locro", 1.63 ),
	   	( "Milanesas con pure", 8.99 );

INSERT INTO pedido ( detalle, fecha, idPlato, idCliente )
VALUES 	( "Lo quiere con papa fritas", '2016-10-23 20:44:11', 1, 1 ),
	   	( "Sin aderezos", '2017-10-23 20:44:11', 2, 1 ),
		( "Con pollo frito", '2018-10-23 20:44:11', 3, 2 ),
	   	( "Sin detalles", '2020-10-23 20:44:11', 4, 2 ),
	   	( "Sin detalles", '2021-10-23 20:44:11', 4, NULL );

-- COUNT - AS - Obtener la cantidad de pedidos.

SELECT COUNT(*) as cantidad_de_clientes 
FROM pedido

-- MIN - AS - Obtener el plato mas barato.

SELECT MIN(precio) as plato_mas_barato 
FROM plato 

-- MAX - AS - Obtener el plato mas caro.

SELECT MAX(precio) as plato_mas_caro
FROM plato 

-- SUM - AS - Obtener la suma de los precios.

SELECT SUM(precio) as suma_de_precios
FROM plato

-- AVG - AS - Obtener el promedio de los.

SELECT AVG(precio) as promedio_de_precios
FROM plato
