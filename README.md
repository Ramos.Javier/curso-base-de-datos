# Curso-Base-de-Datos

Practicas de consultas SQL (querys).

##  1 - Theory

Teoria sobre los siguientes comandos

* ALTER
* USE
* CREATE
* DROP
* INSERT
* UPDATE
* DELETE
* EXPORT CSV
* IMPORT CSV

##  2 - Querys basic

* WHERE
* ORDER BY - ASC - DESC
* GROUP BY
* COUNT - AS
* SUM
* INNER JOIN o JOIN - LEFT JOIN LEFT - RIGHT JOIN

##  3 - Aggregate Functions

* COUNT
* MIN
* MAX
* SUM
* AVG

##  4 - Store Procedures

* CREATE PROCEDURE
* DROP PROCEDURE
* CALL

## 5 - Functions

* CREATE FUNCTION
* DROP FUNCTION
* SELECT

## 6 - Views

* CREATE VIEW
* DROP VIEW
* ALTER VIEW
* SELECT
* LIMIT

## 7 - Triggers

* CREATE TRIGGER
* DROP TRIGGER
