
-- ¿ Que es una views ?

-- Una vista o tabla virtual, es una forma lógica de ver los datos ubicados en varias tablas, es decir, su contenido 
-- esta definido por una consulta. Al igual que una tabla real, una vista consta de filas y columnas pero la vista no 
-- existe como un conjunto de datos almacenados en una base de datos, sino que estos se hacen referencia en la consulta 
-- que define la vista y se produce de forma dinámica cuando se hace referencia a la misma.

-- Una vista también se puede considerar como una Consulta Almacenada ya que sus datos no están almacenados en el mismo, 
-- sino en otras tablas, lo que esta almacenado en la vista, es una instrucción SELECT y el resultado de esta instrucción 
-- forma la tabla virtual que la vista devuelve.

-- Las vistas se utilizan por ejemplo para:

--  * Restringir de la vista del Usuario filas o columnas concretas de una tabla
--  * Combinar columnas de varias tablas de forma que parezca una sola tabla
--  * Restringir la cantidad de datos con las que un usuario puede trabajar

-- Creacion de tablas

Create Table cliente
(
	idCliente Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    telefono varchar(30),
    PRIMARY KEY (idCliente)
);

Create Table plato
(
	idPlato Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    precio decimal (10,2),  -- 10 digitos y 2 decimales
    PRIMARY KEY (idPlato)
);

Create Table pedido
(
	idPedido Int NOT NULL AUTO_INCREMENT,
	detalle TEXT,
	fecha datetime,
    idPlato Int NOT NULL,
    idCliente Int,
    FOREIGN KEY (idPlato) REFERENCES plato (idPlato),
    FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
	PRIMARY KEY (idPedido)
);

-- Insercion de registros.

INSERT INTO cliente ( nombre, telefono )
VALUES 	( "Diego Maradona", "1010101010"),
	   	( "Ernesto Guevara", "1010101011"),
	   	( "Javier Ramos", "1010101100");

INSERT INTO plato ( nombre, precio )
VALUES 	( "Pizza", 2.52),
	   	( "Empanada", 5.36),
		( "Locro", 1.63),
	   	( "Milanesas con pure", 8.99);

INSERT INTO pedido ( detalle, fecha, idPlato, idCliente )
VALUES 	( "Lo quiere con papa fritas", '2016-10-23 20:44:11', 1, 1),
	   	( "Sin aderezos", '2017-10-23 20:44:11', 2, 1),
		( "Con pollo frito", '2018-10-23 20:44:11', 3, 2),
	   	( "Sin detalles", '2020-10-23 20:44:11', 4, 2),
	   	( "Sin detalles", '2021-10-23 20:44:11', 4, NULL);

-- Para crear una vista.

CREATE VIEW dos_pedidos_mas_caro AS
SELECT pe.idCliente AS idCliente, pl.nombre AS nombre, MAX(pl.precio) AS precio
FROM pedido pe, plato pl
WHERE pe.idPlato = pl.idPlato
LIMIT 2;

-- Para modificar una vista.

ALTER VIEW <nombre_vista> AS <SELECT ...>

-- Para eliminar una vista.

DROP VIEW dos_pedidos_mas_caro

-- Para llamar a una vista.

SELECT * FROM dos_pedidos_mas_caro;

