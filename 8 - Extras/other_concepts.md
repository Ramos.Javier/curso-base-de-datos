
# IMPORTANTE : Las pruebas realizadas se hicieron con MariaDB/MySQL, pero existen otros motores de base de datos
# SQL o Postgresql que no solo tienen diferencias de sintaxis, si no funciones que no podremos explicar en este
# curso de BD.

# A continuacion algunos de ellos:


## VISTAS MATERIALIZADAS

Las vistas materializadas es basicamente una tabla en cache, el resultado de la consulta se almacena en una tabla 
caché real, que será actualizada de forma periódica a partir de las tablas originales. Esto proporciona un acceso 
mucho más eficiente, a costa de un incremento en el tamaño de la base de datos y a una posible falta de sincronía, 
es decir, que los datos de la vista pueden estar potencialmente desfasados con respecto a los datos reales. 

## BLOQUES ANONIMOS

Dentro de estos bloques anónimos se pueden emplear instrucciones de programación procedimental de tipo if, while,
... En Mysql no está permitido.

Representacion de un bloque anonimo:

DECLARE
    variable tipo_variable;
BEGIN
    sentencias_a_ejecutar;
END;
