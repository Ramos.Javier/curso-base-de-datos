
-- ¿ Que es una function ?

-- Una función es un conjunto de sentencias que operan como una unidad lógica. Una función tiene un nombre, retorna un 
-- parámetro de salida y opcionalmente acepta parámetros de entrada. Las funciones de SQL Server (SUM, COUNT, AVG, ...)
-- no pueden ser modificadas, las funciones definidas por el usuario si.
-- Las funciones siempre retornan un valor (SOLO UNO).
-- Por ultimo las funciones no pueden ser invocadas por sí solas, mientras que los procedimientos almacenados sí. Por ende,
-- para invocar una función, esta debe estar dentro de una instrucción. 

-- CREATE FUNCTION - Para crear una funcion.

CREATE FUNCTION cuadrado (s SMALLINT) RETURNS SMALLINT
   RETURN s*s;

-- DROP FUNCTION - Para eliminar una funcion.

DROP FUNCTION cuadrado;

-- SELECT in function - Para llamar a una funcion.

SELECT cuadrado(2) AS resultado;

-- **************************************************************************************************************************

-- Creacion de tablas

Create Table cliente
(
	idCliente Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    telefono varchar(30),
    PRIMARY KEY (idCliente)
);

Create Table plato
(
	idPlato Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    precio decimal (10,2),  -- 10 digitos y 2 decimales
    PRIMARY KEY (idPlato)
);

Create Table pedido
(
	idPedido Int NOT NULL AUTO_INCREMENT,
	detalle TEXT,
	fecha datetime,
    idPlato Int NOT NULL,
    idCliente Int,
    FOREIGN KEY (idPlato) REFERENCES plato (idPlato),
    FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
	PRIMARY KEY (idPedido)
);

-- Insercion de registros.

INSERT INTO cliente ( nombre, telefono )
VALUES 	( "Diego Maradona", "1010101010"),
	   	( "Ernesto Guevara", "1010101011"),
	   	( "Javier Ramos", "1010101100");

INSERT INTO plato ( nombre, precio )
VALUES 	( "Pizza", 2.52),
	   	( "Empanada", 5.36),
		( "Locro", 1.63),
	   	( "Milanesas con pure", 8.99);

INSERT INTO pedido ( detalle, fecha, idPlato, idCliente )
VALUES 	( "Lo quiere con papa fritas", '2016-10-23 20:44:11', 1, 1),
	   	( "Sin aderezos", '2017-10-23 20:44:11', 2, 1),
		( "Con pollo frito", '2018-10-23 20:44:11', 3, 2),
	   	( "Sin detalles", '2020-10-23 20:44:11', 4, 2),
	   	( "Sin detalles", '2021-10-23 20:44:11', 4, NULL);

-- Obtenemos la cantidad ($) de compra realizada por el cliente pasado por parametro

CREATE FUNCTION suma_de_compra (idCliente INT) RETURNS DECIMAL(10,2)
   RETURN
	(
		SELECT SUM(pl.precio)
		FROM pedido pe, plato pl
		WHERE pe.idPlato = pl.idPlato and pe.idCliente= idCliente
		GROUP BY pe.idCliente
	);

SELECT suma_de_compra(2) AS gasto_realizado;
