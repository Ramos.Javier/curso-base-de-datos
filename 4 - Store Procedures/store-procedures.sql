
-- ¿ Que es un store procedure ?

-- Los procedimientos almacenados son un conjunto de instrucciones SQL más una serie de estructuras de control que nos 
-- permiten dotar de cierta lógica al procedimiento. Estos procedimientos están guardados en el servidor y pueden ser 
-- accedidos a través de llamadas. Estos procedimientos pueden retornar un valor o no, el decir pueden comportarse como
-- un metodo o una funcion, (las funciones siempre retornan un valor) haciendo una analogia con el desarrollo de software.

-- Creacion de tabla

Create Table cliente
(
	idCliente Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    telefono varchar(30),
    PRIMARY KEY (idCliente)
);

-- Insercion de registros.

INSERT INTO cliente ( nombre, telefono )
VALUES 	( "Diego Maradona", "1010101010"),
	   	( "Ernesto Guevara", "1010101011"),
	   	( "Javier Ramos", "1010101100");

--CREATE PROCEDURE

CREATE PROCEDURE OBTENER_CLIENTE (id INT)
  SELECT * FROM cliente WHERE idCliente = id

-- CALL

CALL OBTENER_CLIENTE(3);

-- DROP PROCEDURE - Para eliminar un procedimiento.

DROP PROCEDURE OBTENER_CLIENTE;

-- Los procedimientos almacenados pueden ser invocados desde el entorno de desarrollo, es decir desde .NET, pero las 
-- funciones no.  Así que al momento de desarrollar, siempre nos comunicaremos con procedimientos almacenados.

-- Imaginemos que la siguiente sentencia puede estar en java. Esto no daria error si el procediemiento ya fue creado en
-- el servidor.

CALL OBTENER_CLIENTE(3);

-- Algo a tener encuenta es que los los stored procedures solamente podemos procesar los valores de tipo escalar, es decir
-- aquellos que puedo pasar por parametros. Dicho de otra manera si mi stored procedure retorna una tabla o conjunto de 
-- tablas, no es posible recuperar esos valores y usarlos para procesarlos dentro de otro código. Por ej. no podemos hacer:
-- SELECT tp.nombre as nombre
-- FROM CALL OBTENER_CLIENTE(3) as tp;
