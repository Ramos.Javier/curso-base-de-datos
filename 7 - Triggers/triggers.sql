
-- ¿ Que es un trigger ?

-- Un trigger también conocido como disparador, es una especie de script en lenguaje de programación SQL, MySQL o 
-- PostgreSQL para base de datos.

-- Consiste en una serie de procedimientos que se ejecutan, según instrucciones definidas, cuando se lleven a cabo 
-- determinadas operaciones, sobre la información que contiene una base de datos.
-- Generalmente, un trigger se acciona cuando se ejecutan acciones para insertar, borrar o modificar los datos de una tabla.

-- ¿ Para que sirve ?

-- Los disparadores o triggers, sirven para gestionar de mejor manera la base datos de manera automática, sin que una 
-- persona tenga que intervenir y sin necesidad de recurrir a lenguajes de programación externos, ya que todo esto ocurre
-- dentro de la misma base de datos.

-- ***************************************************************************************************************************

-- En este simple ejemplo podemos ver que tenemos 2 tablas (animales y contador_de_animales). Sin mucha complicacion, queremos
-- que a partir de la insercion de un animal en 'animales', se nos actualice la tabla 'contador_de_animales'. Esto como bien
-- sabran lo podemos hacer manualmente desde un lenguaje (Java, C#, etc.) pero lo haremos con un trigger.

-- Creacion de tablas

CREATE TABLE animales 
(
	idAnimal mediumint(9) NOT NULL AUTO_INCREMENT, 
	nombre char(30) NOT NULL, 
	PRIMARY KEY (idAnimal)
);

CREATE TABLE contador_de_animales
(
	cantidad int
);

-- Creacion del trigger o disparador

CREATE TRIGGER incrementar_animales 
AFTER INSERT ON animales
FOR EACH ROW
	UPDATE contador_de_animales SET contador_de_animales.cantidad = contador_de_animales.cantidad+1;


-- Inicializamos el contador

INSERT INTO contador_de_animales (cantidad) VALUES(0);

-- AFTER - Contador antes de la insercion.

SELECT * FROM contador_de_animales;

-- Insertamos dos tipos de animales.

INSERT INTO animales (nombre) 
VALUES('lamayoriadelospoliticos'),
		('ylosquevendran');

-- BEFORE - Contador despues de la insercion.

SELECT * FROM contador_de_animales;

-- Para eliminar un trigger.

DROP TRIGGER incrementar_animales;
